﻿namespace AuctionDAL.Enums
{
    /// <summary>
    /// Enum for statuses that Lot can be in.
    /// </summary>
    public enum LotStatus
    {
        Archived,
        Active,
        ReadyForArchive
    }
}
