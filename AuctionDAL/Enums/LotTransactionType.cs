﻿namespace AuctionDAL.Enums
{
    /// <summary>
    /// Enum for types of transaction of LotHistory entity.
    /// </summary>
    public enum LotTransactionType
    {
        Opened,
        Canceled,
        Offer,
        Closed,
        BoughtOut
    }
}