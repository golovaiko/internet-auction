﻿using AuctionDAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AuctionDAL.Context
{
    public class AuctionContext : IdentityDbContext<AuctionUser>
    {
        public DbSet<Lot> Lots { get; set; }
        public DbSet<LotHistory> LotHistories { get; set; }

        public AuctionContext(DbContextOptions<AuctionContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityRole>().HasData(new[]
            {
                new IdentityRole(){ Id= "b148d392-a8a4-4536-a63f-e5e70b4d2d5d", Name = "user", NormalizedName = "USER".ToUpper() },
                new IdentityRole(){ Id = "774b7eb8-2390-4c88-8de8-3d11251af4d1", Name = "admin" , NormalizedName = "ADMIN".ToUpper() }
            });

            var hasher = new PasswordHasher<IdentityUser>();

            modelBuilder.Entity<AuctionUser>().HasData(
                new AuctionUser
                {
                    Id = "1e36c428-04c6-417e-bc83-e71a46892600",
                    UserName = "email@email.com",
                    NormalizedUserName = "EMAIL@EMAIL.COM",
                    Email = "email@email.com",
                    NormalizedEmail = "EMAIL@EMAIL.COM",
                    PasswordHash = hasher.HashPassword(null, "P@sword123"),
                    FirstName = "Roma",
                    LastName = "Holovaiko"
                }
            );

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new[] {
                //user role
                new IdentityUserRole<string>
                {
                    RoleId = "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                    UserId = "1e36c428-04c6-417e-bc83-e71a46892600"
                },
                // admin role
                new IdentityUserRole<string>
                {
                    RoleId = "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                    UserId = "1e36c428-04c6-417e-bc83-e71a46892600"
                }
            });


        }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}
