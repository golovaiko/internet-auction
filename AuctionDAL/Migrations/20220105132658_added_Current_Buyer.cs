﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuctionDAL.Migrations
{
    public partial class added_Current_Buyer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CurrentBuyerId",
                table: "Lots",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "37228f50-fef5-4666-9404-4b817ce333b8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "f04adaf3-8cde-40cf-b37d-beb2f1c7abb2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "489a7142-0283-47eb-aa85-7ddd8bd6c6cc", "AQAAAAEAACcQAAAAEGfpHdfynLGo8wuWb3LFvNDzwNHAGi2j06b459c873RNikoAM5yK9An2VG7XTp2nnQ==", "d1635949-7be7-4ade-bb50-2c83fc1efd4b" });

            migrationBuilder.CreateIndex(
                name: "IX_Lots_CurrentBuyerId",
                table: "Lots",
                column: "CurrentBuyerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_AspNetUsers_CurrentBuyerId",
                table: "Lots",
                column: "CurrentBuyerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_AspNetUsers_CurrentBuyerId",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_CurrentBuyerId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "CurrentBuyerId",
                table: "Lots");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "56c13a51-45f8-45ca-bd16-86f9db65bff3");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "06639b34-0469-4c9c-bc60-6d897696f2a0");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d2a8da24-6730-4e8d-b083-ec00dd11de53", "AQAAAAEAACcQAAAAEIgx9V/eFyWIY8AT/2a257kILOAtD2LU9WxXZrM/D8cxAisUyuQ5tv5hgTWGcOyzkg==", "a68d6c7b-b4fc-47c7-a704-600967b7aacf" });
        }
    }
}
