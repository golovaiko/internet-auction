﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuctionDAL.Migrations
{
    public partial class added_final_price_changed_buyer_to_finalBuyer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_AspNetUsers_BuyerId",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_BuyerId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "BuyerId",
                table: "Lots");

            migrationBuilder.AddColumn<string>(
                name: "FinalBuyerId",
                table: "Lots",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "FinalPrice",
                table: "Lots",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "30fe0a0e-64d8-41ae-bf5c-99012dab5245");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "a724944a-373e-4ea2-a1d2-76c5e4fa9614");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "26f7a6d7-1a15-40c5-ace0-b0cf063d6293", "AQAAAAEAACcQAAAAECucgrPiMZb+7oH4imLVpOwEUKTUyNPhOvUB+HRMl1ea2S2QQJ1S2DKeWEB9setqlQ==", "88d44200-61c0-4fd5-b517-3d181d1c48ea" });

            migrationBuilder.CreateIndex(
                name: "IX_Lots_FinalBuyerId",
                table: "Lots",
                column: "FinalBuyerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_AspNetUsers_FinalBuyerId",
                table: "Lots",
                column: "FinalBuyerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Lots_AspNetUsers_FinalBuyerId",
                table: "Lots");

            migrationBuilder.DropIndex(
                name: "IX_Lots_FinalBuyerId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "FinalBuyerId",
                table: "Lots");

            migrationBuilder.DropColumn(
                name: "FinalPrice",
                table: "Lots");

            migrationBuilder.AddColumn<string>(
                name: "BuyerId",
                table: "Lots",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "12cdb05c-a167-48e3-99bc-494cf1529578");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "9f682d79-944a-4e7c-9c14-37db7c477a70");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b354e4ac-3732-488d-860c-4b063c1c776a", "AQAAAAEAACcQAAAAEOXutDDv2y2vMnlkT+IbGwGLFYDNMo37OZ2IDGzAurB6vG+PQIAoQBDwuheAaAjvwQ==", "dc51dcd0-b584-420c-9647-e84a9756b3ba" });

            migrationBuilder.CreateIndex(
                name: "IX_Lots_BuyerId",
                table: "Lots",
                column: "BuyerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_AspNetUsers_BuyerId",
                table: "Lots",
                column: "BuyerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
