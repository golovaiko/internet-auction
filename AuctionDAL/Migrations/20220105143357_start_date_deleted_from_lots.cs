﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AuctionDAL.Migrations
{
    public partial class start_date_deleted_from_lots : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Lots");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "337c5e04-4ed8-4744-85c6-37f6d0fa2045");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "90959a06-308c-433c-8912-0abb8eaa1f1b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f2accfd2-bc00-4b47-998e-9cfa6d6c5c7a", "AQAAAAEAACcQAAAAECW2EOK9hj3D9C1jF+rxfssqfLeuGa9QB7uXiO7co7oXWbkCt68vUbwE/BAoEG/niw==", "63d5c716-94ca-4641-a8d1-84795afde755" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Lots",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "37228f50-fef5-4666-9404-4b817ce333b8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "f04adaf3-8cde-40cf-b37d-beb2f1c7abb2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "489a7142-0283-47eb-aa85-7ddd8bd6c6cc", "AQAAAAEAACcQAAAAEGfpHdfynLGo8wuWb3LFvNDzwNHAGi2j06b459c873RNikoAM5yK9An2VG7XTp2nnQ==", "d1635949-7be7-4ade-bb50-2c83fc1efd4b" });
        }
    }
}
