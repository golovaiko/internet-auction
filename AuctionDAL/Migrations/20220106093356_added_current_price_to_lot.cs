﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuctionDAL.Migrations
{
    public partial class added_current_price_to_lot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "CurrentPrice",
                table: "Lots",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "12cdb05c-a167-48e3-99bc-494cf1529578");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "9f682d79-944a-4e7c-9c14-37db7c477a70");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b354e4ac-3732-488d-860c-4b063c1c776a", "AQAAAAEAACcQAAAAEOXutDDv2y2vMnlkT+IbGwGLFYDNMo37OZ2IDGzAurB6vG+PQIAoQBDwuheAaAjvwQ==", "dc51dcd0-b584-420c-9647-e84a9756b3ba" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentPrice",
                table: "Lots");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "774b7eb8-2390-4c88-8de8-3d11251af4d1",
                column: "ConcurrencyStamp",
                value: "337c5e04-4ed8-4744-85c6-37f6d0fa2045");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b148d392-a8a4-4536-a63f-e5e70b4d2d5d",
                column: "ConcurrencyStamp",
                value: "90959a06-308c-433c-8912-0abb8eaa1f1b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1e36c428-04c6-417e-bc83-e71a46892600",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f2accfd2-bc00-4b47-998e-9cfa6d6c5c7a", "AQAAAAEAACcQAAAAECW2EOK9hj3D9C1jF+rxfssqfLeuGa9QB7uXiO7co7oXWbkCt68vUbwE/BAoEG/niw==", "63d5c716-94ca-4641-a8d1-84795afde755" });
        }
    }
}
