﻿using AuctionDAL.Enums;
using System;

namespace AuctionDAL.Entities
{
    /// <summary>
    /// Class that represents LotHistory entity for current application.
    /// </summary>
    public class LotHistory
    {
        public int Id { get; set; }
        public int LotId { get; set; }
        public AuctionUser User { get; set; }
        public DateTime TransactionDate { get; set; }
        public LotTransactionType TransactionType { get; set; }
    }
}