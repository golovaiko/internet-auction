﻿using AuctionDAL.Enums;
using System;
using System.Collections.Generic;

namespace AuctionDAL.Entities
{
    /// <summary>
    /// Class that represents Lot entity for current application.
    /// </summary>
    public class Lot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public LotStatus Status { get; set; }

        public double StartPrice { get; set; }
        public double? BuyoutPrice { get; set; }
        public double? CurrentPrice { get; set; }
        public double MinPriceStep { get; set; }
        public double? FinalPrice { get; set; }


        public DateTime CreationDate { get; set; }
        public DateTime EndDate { get; set; }
        
        public AuctionUser Owner { get; set; }
        public AuctionUser CurrentBuyer { get; set; }
        public AuctionUser FinalBuyer { get; set; }
        public ICollection<LotHistory> LotHistory { get; set; }
    }
}
