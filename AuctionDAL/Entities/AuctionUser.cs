﻿using Microsoft.AspNetCore.Identity;

namespace AuctionDAL.Entities
{
    /// <summary>
    /// Class that represents user in current application.
    /// </summary>
    public class AuctionUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
