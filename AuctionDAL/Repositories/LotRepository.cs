﻿using AuctionDAL.Context;
using AuctionDAL.Entities;
using AuctionDAL.Enums;
using AuctionDAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionDAL.Repositories
{
    /// <inheritdoc cref="ILotRepository">
    public class LotRepository : ILotRepository
    {
        private readonly AuctionContext _auctionContext;
        /// <summary>
        /// Constructor to define DbContext through dependency injection.
        /// </summary>
        /// <param name="auctionContext">This applications DbContext.</param>
        public LotRepository(AuctionContext auctionContext)
        {
            _auctionContext = auctionContext;
        }

        public async Task AddAsync(Lot lot)
        {
            _auctionContext.Lots.Add(lot);
            await _auctionContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var lotToArchive = await _auctionContext.Lots.FindAsync(id);
            lotToArchive.Status = LotStatus.Archived;

            _auctionContext.Lots.Update(lotToArchive);
            await _auctionContext.SaveChangesAsync();
        }

        public IQueryable<Lot> FindAllArchived()
        {
            return _auctionContext.Lots
                .Include(x => x.Owner)
                .Include(x => x.FinalBuyer)
                .Include(x => x.CurrentBuyer)
                .Where(x => x.Status == LotStatus.Archived)
                .AsQueryable();
        }

        public IQueryable<Lot> FindAllActive()
        {
            return _auctionContext.Lots
                .Include(x => x.Owner)
                .Include(x => x.FinalBuyer)
                .Include(x => x.CurrentBuyer)
                .Where(lot => lot.Status != LotStatus.Archived)
                .AsQueryable();
        }

        public IQueryable<Lot> FindAllActiveBySearchWord(string searchText)
        {
            return _auctionContext.Lots
                .Include(x => x.Owner)
                .Include(x => x.FinalBuyer)
                .Include(x => x.CurrentBuyer)
                .Where(lot => lot.Status != LotStatus.Archived && 
                (lot.Name.ToUpper().Contains(searchText.ToUpper()) || lot.Description.ToUpper().Contains(searchText.ToUpper())))
                .AsQueryable();
        }

        public async Task<Lot> GetByIdAsync(int id)
        {
            return await _auctionContext.Lots
                .Include(x => x.Owner)
                .Include(x => x.CurrentBuyer)
                .Include(x => x.FinalBuyer)
                .Include(x => x.LotHistory)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Update(Lot entity)
        {
            _auctionContext.Lots.Update(entity);
            await _auctionContext.SaveChangesAsync();
        }
    }
}
