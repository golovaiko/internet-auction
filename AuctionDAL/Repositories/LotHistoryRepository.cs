﻿using AuctionDAL.Context;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionDAL.Repositories
{
    /// <inheritdoc cref="ILotHistoryRepository">
    public class LotHistoryRepository : ILotHistoryRepository
    {
        private readonly AuctionContext _auctionContext;
        /// <summary>
        /// Constructor to define DbContext through dependency injection.
        /// </summary>
        /// <param name="auctionContext">This applications DbContext.</param>
        public LotHistoryRepository(AuctionContext auctionContext)
        {
            _auctionContext = auctionContext;
        }
        public async Task AddAsync(LotHistory lotHistory)
        {
            _auctionContext.LotHistories.Add(lotHistory);
            await _auctionContext.SaveChangesAsync();
        }

        public void AddRange(IEnumerable<LotHistory> lotHistoryList)
        {
            _auctionContext.LotHistories.AddRange(lotHistoryList);
            _auctionContext.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var lotHistoryToRemove = await _auctionContext.LotHistories.FindAsync(id);

            _auctionContext.LotHistories.Remove(lotHistoryToRemove);
            await _auctionContext.SaveChangesAsync();
        }

        public IQueryable<LotHistory> FindAll()
        {
            return _auctionContext.LotHistories.AsQueryable();
        }

        public IQueryable<LotHistory> GetHistoryByLotId(int lotId)
        {
            return _auctionContext.LotHistories.Include(x => x.User).Where(x => x.LotId == lotId).AsQueryable();
        }
    }
}
