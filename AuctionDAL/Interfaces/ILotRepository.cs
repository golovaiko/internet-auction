﻿using AuctionDAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionDAL.Interfaces
{
    /// <summary>
    /// This interface describes actions that can be done with Lot entity.
    /// </summary>
    public interface ILotRepository
    {
        /// <summary>
        /// Method to get all archived lots.
        /// </summary>
        /// <returns>
        /// List of all archived lots.
        /// </returns>
        IQueryable<Lot> FindAllArchived();
        /// <summary>
        /// Method to get all active lots.
        /// </summary>
        /// <returns>
        /// List of all active lots.
        /// </returns>
        IQueryable<Lot> FindAllActive();
        /// <summary>
        /// Method to get all active lots by search string.
        /// </summary>
        /// <returns>
        /// List of all active lots by search string.
        /// </returns>
        IQueryable<Lot> FindAllActiveBySearchWord(string searchText);
        /// <summary>
        /// Method to get Lot by id.
        /// </summary>
        /// <param name="id">Chosen lot id.</param>
        /// <returns>
        /// Chosen lot by id.
        /// </returns>
        Task<Lot> GetByIdAsync(int id);
        /// <summary>
        /// Method adds a new Lot record.
        /// </summary>
        /// <param name="lot">Model of new Lot record.</param>
        Task AddAsync(Lot lot);
        /// <summary>
        /// Method to update existing lot.
        /// </summary>
        /// <param name="lot">Model of lot to update.</param>
        Task Update(Lot lot);
        /// <summary>
        /// Method to delete lot by id.
        /// </summary>
        /// <param name="id">Id of lot to delete.</param>
        Task DeleteByIdAsync(int id);
    }
}
