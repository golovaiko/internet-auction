﻿using AuctionDAL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionDAL.Interfaces
{
    /// <summary>
    /// This interface describes actions that can be done with LotHistory entity.
    /// </summary>
    public interface ILotHistoryRepository
    {
        /// <summary>
        /// Method that returns a list of all existing LotHistories.
        /// </summary>
        /// <returns>
        /// List of all existing LotHistories.
        /// </returns>
        IQueryable<LotHistory> FindAll();
        /// <summary>
        /// Method returns all existing LotHistores for chosen lot.
        /// </summary>
        /// <param name="lotId">Id of chosen lot.</param>
        /// <returns>
        /// List of all existing LotHistories for chosen lot.
        /// </returns>
        IQueryable<LotHistory> GetHistoryByLotId(int lotId);
        /// <summary>
        /// Method adds a new LotHistory.
        /// </summary>
        /// <param name="lotHistory">Model of new LotHistory.</param>
        Task AddAsync(LotHistory lotHistory);
        /// <summary>
        /// Method adds a list of new LotHistory records.
        /// </summary>
        /// <param name="lotHistoryList">List of LotHistory records.</param>
        void AddRange(IEnumerable<LotHistory> lotHistoryList);
        /// <summary>
        /// Method deletes a LotHistory record by id.
        /// </summary>
        /// <param name="id">Id of LotHistory record to delete.</param>
        Task DeleteByIdAsync(int id);
    }
}
