﻿using AuctionBLL.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Internet_auction.Background_Tasks
{
    public class AuctionBackgroundCloser : IHostedService, IDisposable
    {
        private readonly ILogger<AuctionBackgroundCloser> _logger;
        private Timer _timer = null!;
        private readonly IServiceScopeFactory _services;

        public AuctionBackgroundCloser(ILogger<AuctionBackgroundCloser> logger, IServiceScopeFactory services)
        {
            _logger = logger;
            _services = services;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Auction closer service running.");

            _timer = new Timer(CloseLotIfEndDate, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(15));

            return Task.CompletedTask;
        }

        private void CloseLotIfEndDate(object? state)
        {
            try
            {
                using var scope = _services.CreateScope();
                var lotService =
                    scope.ServiceProvider
                        .GetRequiredService<ILotService>();

                var numberOfClosedLots = lotService.CloseLots();
                _logger.LogInformation($"{numberOfClosedLots} lots were closed automatically.");
            }
            catch (Exception e)
            {
                _logger.LogError($"Closing lots failed with exception: {e.Message}");
            }
            
        }


        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Auction closer service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
