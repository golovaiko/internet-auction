﻿using System.ComponentModel.DataAnnotations;

namespace Internet_auction.Models
{
    public class LoginModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
