﻿using System.ComponentModel.DataAnnotations;

namespace Internet_auction.Models
{
    public class AssignUserToRoleModel
    {
        [Required]
        public string Email { get; set; }
        [Required, MinLength(1)]
        public string[] Roles { get; set; }
    }
}
