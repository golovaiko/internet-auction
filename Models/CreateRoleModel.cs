﻿using System.ComponentModel.DataAnnotations;

namespace Internet_auction.Models
{
    public class CreateRoleModel
    {
        [Required, MinLength(4), MaxLength(20)]
        public string RoleName { get; set; }
    }
}
