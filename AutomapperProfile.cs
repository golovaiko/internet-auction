﻿using AuctionDAL.Entities;
using AutoMapper;
using AuctionBLL.DTO;

namespace Internet_auction
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Lot, LotDTO>()
                .ForMember(x => x.OwnerEmail, l => l.MapFrom(lot => lot.Owner.Email))
                .ForMember(x => x.FinalBuyerEmail, l => l.MapFrom(lot => lot.FinalBuyer.Email))
                .ForMember(x => x.CurrentBuyerEmail, l => l.MapFrom(lot => lot.CurrentBuyer.Email));

            CreateMap<LotDTO, Lot>();

            CreateMap<LotHistory, LotHistoryDTO>()
                .ForMember(x => x.UserEmail, lh => lh.MapFrom(h => h.User.Email));

            CreateMap<LotHistoryDTO, LotHistory>();

            CreateMap<LotSimpleDTO, Lot>()
                .ReverseMap();

            CreateMap<AuctionUser, AuctionUserDTO>()
                .ForMember(x => x.Email, s => s.MapFrom(user => user.Email)); 
        }
    }
}
