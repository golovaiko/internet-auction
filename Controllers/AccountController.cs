﻿using AuctionBLL.Account;
using Internet_auction.Helpers;
using Internet_auction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace Internet_auction.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly JwtSettings _jwtSettings;

        public AccountController(
            IUserService userService,
            IOptionsSnapshot<JwtSettings> jwtSettings)
        {
            _userService = userService;
            _jwtSettings = jwtSettings.Value;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            var isRegistered = await _userService.Register(new Register
            {
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Password = model.Password,
            });

            if (!isRegistered)
            {
                return BadRequest("Username already taken!");
            }

            return Created(string.Empty, string.Empty);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _userService.Login(new Login
            {
                Email = model.Email,
                Password = model.Password
            });

            if (user is null) return BadRequest("Wrong username or password!");

            var roles = await _userService.GetRoles(user);

            return Ok(new
            {
                accessToken = JwtHelper.GenerateJwt(user, roles, _jwtSettings),
                firstName = user.FirstName,
                lastName = user.LastName,
                email = user.UserName,
                roles
            });
        }

        [HttpPost("createRole")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> CreateRole(CreateRoleModel model)
        {
            await _userService.CreateRole(model.RoleName);
            return Ok();
        }

        [HttpGet("getRoles")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetRoles()
        {
            return Ok(await _userService.GetRoles());
        }

        [HttpPost("assignUserToRole")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> AssignUserToRole(AssignUserToRoleModel model)
        {
            await _userService.AssignUserToRoles(new AssignUserToRoles
            {
                Email = model.Email,
                Roles = model.Roles
            });

            return Ok();
        }

        [HttpGet("getAllUsers")]
        [Authorize(Roles ="admin")]
        public async Task<IActionResult> GetAllUsers()
        {
            return Ok(await _userService.GetAllUsers());
        }

    }
}
