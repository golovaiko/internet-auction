﻿using AuctionBLL.Interfaces;
using AuctionBLL.DTO;
using AuctionBLL.Account;
using AuctionDAL.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Internet_auction.DTO;
using AutoMapper;

namespace Internet_auction.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LotController : ControllerBase
    {
        private readonly ILotService _lotService;
        private readonly ILotHistoryService _lotHistoryService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public LotController(ILotService lotService, ILotHistoryService lotHistoryService, IUserService userService, IMapper mapper)
        {
            _lotService = lotService;
            _lotHistoryService = lotHistoryService;
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet("getSimpleLots")]
        public IActionResult GetAllActiveLotsSimple()
        {
            return Ok(_lotService.FindAllActiveSimple());
        }

        [HttpGet("getArchivedSimpleLots")]
        [Authorize(Roles = "admin")]
        public IActionResult GetAllArchivedLotsSimple()
        {
            return Ok(_lotService.FindAllArchivedSimple());
        }

        [HttpGet("getSimpleLotsBySearchWord")]
        public IActionResult GetAllActiveLotsBySearchWord([FromQuery] string searchWord)
        {
            return Ok(_lotService.FindAllActiveBySearchWord(searchWord));
        }

        [HttpPost("createLot")]
        [Authorize]
        public async Task<IActionResult> CreateLot(LotDTO dto)
        {
            var user = await _userService.GetUserByEmailAsync(dto.OwnerEmail);

            var lotModel = _mapper.Map<LotDTO>(dto);

            lotModel.Status = LotStatus.Active;
            lotModel.CreationDate = DateTime.UtcNow;

            var createdLotId = await _lotService.AddAsync(lotModel, user);

            await _lotHistoryService.AddAsync(new LotHistoryDTO()
            {
                LotId = createdLotId,
                TransactionDate = DateTime.UtcNow,
                TransactionType = LotTransactionType.Opened,
                UserEmail = user.Email
            }, user);
            return Ok();
        }

        [HttpGet("getLotById/{id}")]
        [Authorize]
        public async Task<IActionResult> GetLotById(int id)
        {
            var lot = await _lotService.GetByIdAsync(id);

            if (lot == null)
            {
                return NotFound("Lot not found!");
            }

            return Ok(lot);
        }

        [HttpPost("makeAnOfferOnLot")]
        [Authorize]
        public async Task<IActionResult> MakeAnOfferOnLot(LotOfferDTO dto)
        {
            var buyer = await _userService.GetUserByEmailAsync(dto.OffererEmail);
            var lot = await _lotService.GetByIdAsync(dto.Id);

            if (lot.EndDate <= DateTime.UtcNow)
            {
                return BadRequest("Auction on this lot is closed!");
            }
            if (dto.OffererEmail == lot.OwnerEmail)
            {
                return BadRequest("Owner is not allowed to offer!");
            }
            if ((dto.OfferedPrice - lot.CurrentPrice) < lot.MinPriceStep
                || dto.OfferedPrice < lot.StartPrice
                || dto.OfferedPrice >= lot.BuyoutPrice)
            {
                return BadRequest("Wrong price!");
            }

            lot.CurrentBuyerEmail = buyer.Email;
            lot.CurrentPrice = dto.OfferedPrice;

            await _lotService.UpdateForOffer(lot, buyer);

            await _lotHistoryService.AddAsync(new LotHistoryDTO()
            {
                LotId = dto.Id,
                TransactionDate = DateTime.UtcNow,
                TransactionType = LotTransactionType.Offer,
            }, buyer);

            return Ok();
        }

        [HttpPost("cancelLot")]
        [Authorize]
        public async Task<IActionResult> CancelLot(LotCancelacionDTO dto)
        {
            var owner = await _userService.GetUserByEmailAsync(dto.OwnerEmail);

            await _lotHistoryService.AddAsync(new LotHistoryDTO()
            {
                LotId = dto.Id,
                TransactionDate = DateTime.UtcNow,
                TransactionType = LotTransactionType.Canceled,
            }, owner);

            return Ok();
        }

        [HttpPost("archiveLot")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> ArchiveLot(LotArchiveDTO dto)
        {
            var lot = await _lotService.GetByIdAsync(dto.Id);

            if (lot.Status == LotStatus.Archived)
            {
                BadRequest("Lot already archived!");

            }

            if (lot.Status != LotStatus.ReadyForArchive)
            {
                return BadRequest("Lot must have status 'Ready to be archived'!");
            }

            await _lotService.DeleteByIdAsync(dto.Id);

            return Ok();
        }

        [HttpPost("buyoutLot")]
        [Authorize]
        public async Task<IActionResult> BuyoutLot(LotBuyoutDTO dto)
        {
            var lot = await _lotService.GetByIdAsync(dto.Id);

            if (lot.EndDate <= DateTime.UtcNow)
            {
                return BadRequest("Auction on this lot is closed!");
            }
            if (dto.BuyerEmail == lot.OwnerEmail)
            {
                return BadRequest("Owner is not allowed to buyout!");
            }

            var buyer = await _userService.GetUserByEmailAsync(dto.BuyerEmail);

            lot.FinalPrice = dto.Price;

            await _lotService.UpdateForBuyout(lot, buyer);

            await _lotHistoryService.AddAsync(new LotHistoryDTO()
            {
                LotId = dto.Id,
                TransactionDate = DateTime.UtcNow,
                TransactionType = LotTransactionType.BoughtOut,
            }, buyer);

            return Ok();
        }

        [HttpGet("getHistoryForLot/{id}")]
        [Authorize]
        public async Task<IActionResult> GetHistoryByLotId(int id)
        {
            return Ok(_lotHistoryService.GetHistoryByLotId(id));
        }
    }
}