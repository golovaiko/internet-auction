export const Statuses = Object.freeze({
    0: "Opened",
    1: "Canceled",
    2: "Offer",
    3: "Closed",
    4: "Bought"
  });