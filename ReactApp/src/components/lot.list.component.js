import LotCard from "./lot.component";
import LoadingIndicator from './loading.indicator.component';

export default function LotList({lots, userLoggedIn})
{
    return (
        lots ? (
            <div>
            {lots.map((lot) => (
                <LotCard key={lot.id} id={lot.id} detailsEnabled={userLoggedIn} title={lot.name} description={lot.description}/>
            ))}
            </div>
        ) : (<LoadingIndicator/>)
    )
} 