import React from "react";
import AuthService from "../services/auth.service";

export default function Profile() {
  const currentUser = AuthService.getCurrentUser();

    return (
      <div className="container">
        <header className="jumbotron">
          <h3>
            <strong>{currentUser.email}</strong> Profile
          </h3>
        </header>
        <p>
          <strong>Token:</strong>{" "}
          {currentUser.accessToken.substring(0, 20)} ...{" "}
          {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
        </p>
        <p>
          <strong>Email:</strong>{" "}
          {currentUser.email}
        </p>
        {currentUser.firstName ?(
          <p>
            <strong>First Name:</strong>{" "}
            {currentUser.firstName}
          </p>
          ): null
        }
        {currentUser.lastName ? (
          <p>
            <strong>Last Name:</strong>{" "}
            {currentUser.lastName}
          </p>
        ): null
        }
        <strong>Authorities:</strong>
        <ul>
          {currentUser.roles &&
            currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
        </ul>
      </div>
    );
}