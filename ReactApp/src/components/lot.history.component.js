import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import LoadingIndicator from './loading.indicator.component';
import LotsService from '../services/lots.service';
import { Statuses } from "../enums/statuses";
import moment from 'moment';
import Tooltip from '@mui/material/Tooltip';
import { useNavigate } from "react-router-dom";
import { validateResponse } from "../helpers/request.error.handler";

export default function HistoryTable({id}) {
    const [lotHistory, setLotHistory] = useState();
    const navigate = useNavigate();
    
    const fetchLotHistory = async (id) => {
        const { data, errorMessage } = await LotsService.getLotHistoryById(id);
        validateResponse(errorMessage, navigate);
        setLotHistory(data);
      };

      useEffect(() => {
        fetchLotHistory(id);
      }, []);

    return (
    lotHistory ? (
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell >User email</TableCell>
              <TableCell align="right">Transaction Type</TableCell>
              <TableCell align="right">Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {lotHistory.map((row, index) => (
              <TableRow
                key={index}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row.userEmail ? row.userEmail : "Lot was closed automatically by system."}
                </TableCell>
                <TableCell align="right">{Statuses[row.transactionType]}</TableCell>
                <Tooltip title={moment.utc(row.transactionDate).local().format('MMMM Do YYYY, h:mm:ss a')} placement="bottom-end">
                    <TableCell align="right">{moment.utc(row.transactionDate).local().fromNow()}</TableCell>
                </Tooltip>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      ) : (<LoadingIndicator/>)
    );
  }