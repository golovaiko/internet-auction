import React, { useState } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import { isEmail } from "validator";
import { useNavigate } from 'react-router-dom';
import AuthService from "../services/auth.service";
import Alert from '@mui/material/Alert';

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const vemail = value => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const vpassword = value => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        The password must be between 6 and 40 characters.
      </div>
    );
  }
};

export default function Register() {
    
  let navigate = useNavigate();
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [passwordConfirm, setpasswordConfirm] = useState("");
  let [firstName, setFirstName] = useState("");
  let [lastName, setLastName] = useState("");
  let [successful, setSuccessful] = useState(false);
  let [message, setMessage] = useState("");

  function onChangePasswordConfirm(e) {
    setpasswordConfirm(e.target.value)
  }

  function onChangeEmail(e) {
    setEmail(e.target.value);
  }

  function onChangePassword(e) {
    setPassword(e.target.value);
  }

  function onChangeFirstName(e) {
    setFirstName(e.target.value);
  }
  
  function onChangeLastName(e) {
    setLastName(e.target.value);
  }

  function handleRegister(e) {
    e.preventDefault();

    setMessage("");
    setSuccessful(true);

    if (!isEmail(email) || password === "" || password !== passwordConfirm) {
      setMessage("Errors in input!");
      setSuccessful(false);
      return;
    }

      AuthService.register(
        email,
        password,
        passwordConfirm,
        firstName,
        lastName
      ).then(
        () => {
          setMessage("Successful");
          setSuccessful(true);
          setTimeout(function() {
            navigate("/login");
          }, 1000);
        },
        error => {
          const resMessage =
            error.response.data.toString();
          
          setMessage(resMessage);
          setSuccessful(false);
        }
      );
    }

    return (
      <div className="col-md-12">
        <div className="card card-container">
          <img
            src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
            alt="profile-img"
            className="profile-img-card"
          />

          <Form
            onSubmit={handleRegister}
          >
              <div>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="email"
                    value={email}
                    onChange={onChangeEmail}
                    validations={[required, vemail]}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="firstName">First Name(optional)</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="firstName"
                    value={firstName}
                    onChange={onChangeFirstName}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="lastName">Last Name(optional)</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="lastName"
                    value={lastName}
                    onChange={onChangeLastName}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="password"
                    value={password}
                    onChange={onChangePassword}
                    validations={[required, vpassword]}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="passwordConfirm">Confrim password</label>
                  <Input
                    type="password"
                    className="form-control"
                    name="passwordConfirm"
                    value={passwordConfirm}
                    onChange={onChangePasswordConfirm}
                    validations={[required, vpassword]}
                  />
                </div>

                <div className="form-group" style={{marginTop: '1em'}}>

                  <button
                    className="btn btn-outline-primary"
                    disabled={successful}
                  >
                    {successful && (
                      <span className="spinner-border spinner-border-sm"></span>
                    )}
                    <span>Register</span>
                  </button>
                </div>
              </div>
            
            {message && 
            (<Alert sx={{mt:2}} severity= {successful ? "success" : "error"}>{message}</Alert>)
            }
          </Form>
        </div>
      </div>
    );
}