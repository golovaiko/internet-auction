import { LinearProgress, Box } from '@mui/material';

export default function LoadingIndicator() {
    return (
        <Box>
            <LinearProgress/>
        </Box>
    )
}