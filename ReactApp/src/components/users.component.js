import { useState, useEffect } from "react";
import AuthService from "../services/auth.service";
import LoadingIndicator from '../components/loading.indicator.component';
import { Typography } from "@mui/material";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { useNavigate } from "react-router-dom";
import { validateResponse } from "../helpers/request.error.handler";

export default function Users() {
    const [users, setUsers] = useState();
    const navigate = useNavigate();

    const fetchUsers = async () => {
        const { data , errorMessage } = await AuthService.getAllUsers();
        validateResponse(errorMessage , navigate)
        setUsers(data);
    };

    useEffect(() => {
        fetchUsers();
    }, []);

    return  users ? 
            <div>
            {users.map((user, index) => (
                <Card key={index} sx={{
                    boxShadow: 2,
                    border: 2,
                    borderColor: 'primary.light',
                    '& .MuiDataGrid-cell:hover': {
                        color: 'primary.main',
                    },
                    m: 2,
                    }}>
                    <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {user.email}
                    </Typography>
                    {user.firstName ?
                        (<Typography variant="body2" color="text.secondary">
                            <strong>First name:</strong> {user.firstName}     
                        </Typography>
                        ) : null
                    }
                    {user.lastName ?
                        (<Typography variant="body2" color="text.secondary">
                            <strong>Last name:</strong> {user.lastName}
                        </Typography>
                        ) : null
                    }
                    </CardContent>
                </Card>))}
            </div>
         : (<LoadingIndicator/>)
}