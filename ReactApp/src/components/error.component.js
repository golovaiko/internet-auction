import { Card, CardActions, CardContent, Link, Typography } from '@mui/material';
import { useSearchParams } from "react-router-dom";

export default function Error()
{
    const [searchParams, setSearchParams] = useSearchParams();
    var message = searchParams.get("error")

    return (
    <div>
        <Card sx={{
                boxShadow: 2,
                border: 2,
                borderColor: 'error.main',
                m: 2,
                }}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    Something went wrong!
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {message}
                </Typography>
            </CardContent>
            <CardActions>
                <Link href="/">Home</Link>
            </CardActions>
        </Card>
    </div>
    )
}