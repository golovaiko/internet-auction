import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import LotsService from '../services/lots.service';
import React, { useState } from "react";
import Box from '@mui/material/Box';
import { useNavigate } from 'react-router-dom';
import AuthService from "../services/auth.service";
import moment from 'moment';
import 'moment-timezone';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateTimePicker from '@mui/lab/DateTimePicker';
import DateAdapter from '@mui/lab/AdapterMoment';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

export default function LotCreate() {
  const currentUser = AuthService.getCurrentUser()
  let ownerEmail = "";
  let navigate = useNavigate();
  let [name, setName] = useState("");
  let [startPrice, setStartPrice] = useState("");
  let [buyoutPrice, setBuyoutPrice] = useState("");
  let [minPriceStep, setMinPriceStep] = useState("");
  let [endDate, setEndDate] = useState(moment().add(1, 'days'));
  let [description, setDescription] = useState("");
  let [message, setMessage] = useState("");

  function onChangeName(e) {
    setName(e.target.value)
  }

  function onChangeStartPrice(e) {
    setStartPrice(e.target.value)
  }

  function onChangeBuyOutPrice(e) {
    setBuyoutPrice(e.target.value)
  }

  function onChangeMinPriceStep(e) {
    setMinPriceStep(e.target.value)
  }

  const onChangeEndDate = (value) => {
    setEndDate(moment.tz(value, Intl.DateTimeFormat().resolvedOptions().timeZone).utc().format())
  }

  function onChangeDescription(e) {
    setDescription(e.target.value)
  }

  function handleCreation(e){
    e.preventDefault();

    setMessage("");

    ownerEmail  = currentUser.email;

    if (
      name === "" || 
      description === "" || 
      startPrice === "" || 
      isNaN(startPrice) || 
      isNaN(buyoutPrice) ||
      minPriceStep === "" ||
      isNaN(minPriceStep) ||
      endDate === "" || 
      ownerEmail === "") {
      setMessage("Errors in input!");
      return;
    }

    var utcEndDate = moment.tz(endDate, Intl.DateTimeFormat().resolvedOptions().timeZone).utc().format();

    LotsService.createLot(
      name, 
      description , 
      startPrice, 
      buyoutPrice, 
      minPriceStep, 
      utcEndDate, 
      ownerEmail
    ).then(
      () => {
        navigate("/");
      },
      error => {
        const resMessage =
          error.response.data.toString();
        
        setMessage(resMessage);
      }
    );
  }

  return (
    <Card sx={{
      boxShadow: 2,
      border: 2,
      borderColor: 'primary.light',
      '& .MuiDataGrid-cell:hover': {
        color: 'primary.main',
      },
      m: 2,
    }}>
    <CardContent >
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { m: 1, width: '30ch' },
        }}
        noValidate
        autoComplete="off"
      >
        <Stack 
          direction="column"
          justifyContent="flex-start"
          alignItems="flex-start"
          spacing={3}    
        >
          <Typography gutterBottom variant="h5" component="div">Create lot:</Typography>
          <TextField 
            required
            label="Name:"
            name="name"
            value={name}
            onChange={onChangeName}
            validations={[required]}
            focused
          />
          <TextField 
            required
            label="Start price:" 
            name="startPrice"
            value={startPrice}
            onChange={onChangeStartPrice}
            validations={[required]}
            focused
            />
          <TextField 
            label="Buyout price(optional):" 
            name="buyoutPrice"
            value={buyoutPrice}
            onChange={onChangeBuyOutPrice}
            focused
          />
          <TextField 
            required
            label="Minimal bet amount:" 
            name="minPriceStep"
            value={minPriceStep}
            onChange={onChangeMinPriceStep}
            validations={[required]}
            focused
          />
          <LocalizationProvider dateAdapter={DateAdapter}>
            <DateTimePicker
              name='endDate'
              label="Date of auction end:"
              value={endDate}
              onChange={onChangeEndDate}
              minDate={moment().add(1, 'days')}
              maxDate={moment().add(30, 'days')}
              renderInput={(params) => <TextField required focused {...params} />}
            />
          </LocalizationProvider>
          <TextField 
            required
            label="Description:"
            name="description"
            value={description}
            onChange={onChangeDescription}
            validations={[required]}
            focused
          />

          {message && (
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
          )}
          <CardActions sx={{p:0}}>
            <Button variant="outlined" color="primary" onClick={handleCreation} >Create</Button>
          </CardActions>
        </Stack>
      </Box>
    </CardContent>
  </Card>
  );
}
