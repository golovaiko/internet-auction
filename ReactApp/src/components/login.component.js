import { useNavigate } from 'react-router-dom';
import React, { useState } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import { isEmail } from "validator";
import Alert from '@mui/material/Alert';

import AuthService from "../services/auth.service";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

export default function Login() {
    let navigate = useNavigate();
    let [username, setUsername] = useState("");
    let [password, setPassword] = useState("");
    let [loading, setLoading] = useState(false);
    let [message, setMessage] = useState("");

  function onChangeUsername(e) {
    setUsername(e.target.value);
  }

  function onChangePassword(e) {
    setPassword(e.target.value);
  }

  function handleLogin(e) {
    e.preventDefault();
    
    setMessage("");
    setLoading(true);

    if(!isEmail(username) || password === "")
    {
      setMessage("Errors in input!");
      setLoading(false);
      return;
    }
      AuthService.login(username, password).then(
        () => {
          navigate("/");
          window.location.reload();
        },
        error => {
          const resMessage =
            error.response.data.toString();
          
          setMessage(resMessage);
          setLoading(false);
        }
      );
  }

    return (
      <div className="col-md-12">
        <div className="card card-container">
          <img
            src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
            alt="profile-img"
            className="profile-img-card"
          />
          <Form
            onSubmit={handleLogin}
          >
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <Input
                type="text"
                className="form-control"
                name="username"
                value={username}
                onChange={onChangeUsername}
                validations={[required]}
              />
            </div>

            <div className="form-group">
              <label htmlFor="password">Password</label>
              <Input
                type="password"
                className="form-control"
                name="password"
                value={password}
                onChange={onChangePassword}
                validations={[required]}
              />
            </div>

            <div className="form-group" style={{marginTop: '1em'}}>
              <button
                className="btn btn-outline-primary"
                disabled={loading}
              >
                {loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Login</span>
              </button>
            </div>

            {message && 
              (<Alert sx={{mt:2}} severity="error">{message}</Alert>)
            }
          </Form>
        </div>
      </div>
    );
}