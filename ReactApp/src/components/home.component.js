import React from "react";
import Lots from "./lots.component";
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import { useNavigate } from 'react-router-dom';
import AuthService from "../services/auth.service";

export default function Home() {
  let navigate = useNavigate();
  const currentUser = AuthService.getCurrentUser();
  
  function onCreateClick() {
    navigate("/lot/createLot");
  };

  function onSearchClick(){
    navigate('/lot/search');
  }

  return (
    <div className="container">
      <Stack direction="row" sx={{m:2}} justifyContent="space-between">
        {currentUser ? 
          (<Button variant="outlined" onClick={onCreateClick}>Create Lot</Button>) : null
        }
        <Button variant="outlined" onClick={onSearchClick}>Search</Button>
      </Stack>
      <Lots/>
    </div>
  );
}