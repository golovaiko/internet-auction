import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useParams } from 'react-router-dom';
import LotsService from '../services/lots.service';
import React, { useState, useEffect } from "react";
import { Input } from '@mui/material';
import LoadingIndicator from './loading.indicator.component';
import AuthService from '../services/auth.service';
import Stack from '@mui/material/Stack';
import { useNavigate } from 'react-router-dom';
import Collapse from '@mui/material/Collapse';
import LotHistory from "../components/lot.history.component";
import { Statuses } from "../enums/statuses";
import moment from 'moment';
import Tooltip from '@mui/material/Tooltip';
import { validateResponse } from "../helpers/request.error.handler";

export default function LotDetails() {
  const currentUser = AuthService.getCurrentUser()
  const urlParams = useParams();
  const [lot, setLot] = useState();
  let navigate = useNavigate();
  let [offer, setOffer] = useState("");
  let [message, setMessage] = useState("");
  const [checked, setChecked] = useState(false);

  const fetchLotDetails = async (id) => {
    const { data, errorMessage } = await LotsService.getLotDetailsById(id);
    validateResponse(errorMessage, navigate);
    setLot(data);
  };
  
  useEffect(() => {
    fetchLotDetails(urlParams.id);
  }, []);

  function isReadyForArchive()
  {
     return moment.utc(lot.endDate) <= moment.utc();
  }

  function onOfferChange(e) {
    setOffer(e.target.value)
  }

  function handleCollapse(){
    setChecked((prev) => !prev);
  }

  function handleCanceling(){
    LotsService.cancelLot(lot.id, lot.ownerEmail)
    .then(
      () => {
        navigate("/");
      },
      error => {
        const resMessage =
          error.response.data.toString();
        
        setMessage(resMessage);
      }
    );
  }

  function handleArchive(){
    LotsService.archiveLot(lot.id)
    .then(
      () => {
        navigate('/');
      },
      error => {
        const resMessage =
          error.response.data.toString();
        
        setMessage(resMessage);
      }
    );
  }
  
  function handleBuying(){
    LotsService.buyoutLot(lot.id, currentUser.email, lot.buyoutPrice)
    .then(
      () => {
        window.location.reload();
      },
      error => {
        const resMessage =
          error.response.data.toString();
        
        setMessage(resMessage);
      }
    );
  }

  function handleOffering(e){
    e.preventDefault();

    setMessage("");

    if(isNaN(offer) || offer === "")
      {
        setMessage("Offer must be and must be a number!");
        return;
      }
    
      if(!lot.currentPrice ? ( +offer - lot.startPrice < lot.minPriceStep) :( +offer - lot.currentPrice < lot.minPriceStep))
      {
        setMessage("Offer is too low!");
        return;
      }

    LotsService.makeAnOfferOnLot(
      lot.id,
      offer , 
      currentUser.email,
    ).then(
      () => {
        window.location.reload();
      },
      error => {
        const resMessage =
          error.response.data.toString();
        
        setMessage(resMessage);
      }
    );
    
  }

  return lot ? (
    <Card sx={{
      boxShadow: 2,
      border: 2,
      borderColor: 'primary.light',
      '& .MuiDataGrid-cell:hover': {
        color: 'primary.main',
      },
      m: 2,
    }}>
    <CardContent>
      <Typography gutterBottom variant="h5" component="div">
        {lot.name}
      </Typography>
      <Typography variant="body2">
        Status: {Statuses[lot.lastTransaction]}     
      </Typography>
      <Typography variant="body2">
        Owner: {lot.ownerEmail}
      </Typography>
      <Tooltip title={moment.utc(lot.endDate).local().format('MMMM Do YYYY, h:mm:ss a')} placement="bottom-start">
        <Typography variant="body2">
          End date: {moment.utc(lot.endDate).local().fromNow()}
        </Typography>
      </Tooltip>
      <Typography variant="body2">
        Description: {lot.description}     
      </Typography>
      {lot.finalPrice === null && (lot.lastTransaction === 0 || lot.lastTransaction === 2) ? (
        <Typography variant="body2">
        Start price: {lot.startPrice}     
        </Typography>
        ) : null
      }
      {lot.buyoutPrice && lot.lastTransaction !== 3 && lot.lastTransaction !== 4? (
        <Typography variant="body2">
          Buyout price: {lot.buyoutPrice}     
        </Typography>
        ): null
      }

      {lot.finalPrice && (lot.lastTransaction === 3 ||  lot.lastTransaction === 4) ? 
        (
          <Typography variant="body2">
            Final price: {lot.finalPrice}
          </Typography>
        ) : null
      }

      {lot.finalPrice === null && (lot.lastTransaction === 0 || lot.lastTransaction === 2) ? (
        <Typography variant="body2">
          Current buyer: {lot.currentBuyerEmail}
        </Typography>
        ) : null
      }
      {lot.finalPrice === null && (lot.lastTransaction === 0 || lot.lastTransaction === 2) ? (
        <Typography variant="body2">
          Minimal bet: {!lot.currentPrice ? (lot.startPrice + lot.minPriceStep) : (lot.currentPrice + lot.minPriceStep) }     
        </Typography>
        ) : null
      }
      {lot.finalPrice === null && (lot.lastTransaction === 0 || lot.lastTransaction === 2)? (
        <Typography variant="body2">
          Current bet: {lot.currentPrice}
        </Typography>
        ) : null
      }
      {lot.finalBuyerEmail ? (
      <Typography variant="body2">
        Final buyer: {lot.finalBuyerEmail}
      </Typography>) : null
       }
      {message && (
          <div className="alert alert-danger" role="alert">
            {message}
          </div>
      )}
    </CardContent>
    <CardActions>
      <Stack direction="row" spacing={2}>
        {(currentUser.email !== lot.ownerEmail && (lot.lastTransaction === 0 || lot.lastTransaction === 2) && lot.status !== 0) ? (
          <Input 
            disabled={isReadyForArchive()}
            name="offer"
            value={offer}
            placeholder='Offer:'
            onChange={onOfferChange}
          />
          ) : null}
        {(currentUser.email !== lot.ownerEmail && (lot.lastTransaction === 0 || lot.lastTransaction === 2) && lot.status !== 0)? (
          <Button disabled={isReadyForArchive()} variant="outlined" color="primary" onClick={handleOffering} >Offer</Button>
        ): null }
        {currentUser.email === lot.ownerEmail && (lot.lastTransaction === 0 || lot.lastTransaction === 2) && lot.status !== 0 ? (
          <Button variant="outlined" color="primary" onClick={handleCanceling} >Cancel</Button>
        ) : null
        }
        {(currentUser.email !== lot.ownerEmail && (lot.lastTransaction === 0 || lot.lastTransaction === 2) && lot.buyoutPrice && lot.status !== 0 ) ? (
          <Button disabled={isReadyForArchive()} variant="outlined" color="primary" onClick={handleBuying}>Buyout</Button>
        ): null }
        {(currentUser.roles[0] === "admin" && lot.status === 2) ? (
          <Button variant="outlined" color="primary" onClick={handleArchive} >Archive</Button>
        ) : null
        }
        {currentUser ? (
          <Button variant="outlined" color="primary" onClick={handleCollapse}>History</Button>
        ) : null
        }
        </Stack>
    </CardActions>
    <Collapse in={checked}>{<LotHistory id={urlParams.id} />}</Collapse>
  </Card>
  ) : (<LoadingIndicator/>);
}
