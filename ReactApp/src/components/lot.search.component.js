import React, { useState, useEffect } from "react";
import LotsService from '../services/lots.service';
import AuthService from "../services/auth.service";
import LotList from "../components/lot.list.component";
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import TextField from "@mui/material/TextField";
import Button from '@mui/material/Button';
import { isEmpty } from "validator";
import LoadingIndicator from './loading.indicator.component';
import Alert from '@mui/material/Alert';
import { useNavigate } from "react-router-dom";
import { validateResponse } from "../helpers/request.error.handler";

export default function LotsSearch() {
    let [searchWord, setSearchText] = useState("");
    let [isSearchEmpty, setIsSearchEmpty] = useState(false);
    let [lotsSearch, setLotsSearch] = useState([]);
    let [loading , setLoading] = useState(false);
    let [nothingFound , setNothingFound] = useState(false);
    const [userLoggedIn, setUserLoggedIn] = useState(false);
    const navigate = useNavigate();
    
    const fetchSearchLots = async () => {
        setLoading(true);
        const { data, errorMessage } = await LotsService.getSearchLots(searchWord);
        validateResponse(errorMessage , navigate);
        setLoading(false);
        setNothingFound(data.length === 0);
        setLotsSearch(data);
    };

    useEffect(() => {
        setUserLoggedIn(AuthService.getCurrentUser() ? true : false)
    }, []);

    function onChangeSearchText(e) {
        setSearchText(e.target.value);
    }

    function onSearchClick(){
        if(isEmpty(searchWord.trim())){
        setIsSearchEmpty(true);
        return;
        }
        setIsSearchEmpty(false);
        fetchSearchLots();
    }

    return (!loading ? (
            <Box>
                <Stack direction="row" sx={{m:2}} justifyContent="space-between">
                    <TextField size="small" label="Search:" color= {isSearchEmpty ? "error" : "primary"} focused onChange={onChangeSearchText}/>
                    <Button variant="outlined" onClick={onSearchClick}>Search</Button>
                </Stack>
                {!nothingFound ? 
                    (<LotList lots={lotsSearch} userLoggedIn={userLoggedIn} />): 
                    (<Alert sx={{m:2}} severity="info">Nothing found!</Alert>)
                }
            </Box>
        ) : (<LoadingIndicator/>)
    )
}