import React, { useState, useEffect } from "react";
import LotsService from '../services/lots.service';
import AuthService from "../services/auth.service";
import LotList from "../components/lot.list.component";
import { useNavigate } from "react-router-dom";
import { validateResponse } from "../helpers/request.error.handler";

export default function Lots() {
  const [lots, setLots] = useState();
  const [userLoggedIn, setUserLoggedIn] = useState(false);
  const navigate = useNavigate();

  const fetchLots = async () => {
    const { data, errorMessage } = await LotsService.getPublicContent();
    validateResponse(errorMessage , navigate);
    setLots(data);
  };

  useEffect(() => {
    setUserLoggedIn(AuthService.getCurrentUser() ? true : false)
    fetchLots();
  }, []);

  return (
    <LotList lots={lots} userLoggedIn={userLoggedIn} />
  )
}