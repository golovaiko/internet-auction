import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useNavigate } from 'react-router-dom';

export default function LotCard(props) {
  let navigate = useNavigate();

  function onDetailsClick(id) {
    navigate(`/lot/${id}`);
  };

  return (
    <Card sx={{
        boxShadow: 2,
        border: 2,
        borderColor: 'primary.light',
        '& .MuiDataGrid-cell:hover': {
          color: 'primary.main',
        },
        m: 2,
      }}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {props.description}     
        </Typography>
      </CardContent>
      <CardActions>
        {props.detailsEnabled ?  <Button variant="outlined" color="primary" onClick={() => {onDetailsClick(props.id)}}>Details</Button> : null}
      </CardActions>
    </Card>
  );
}
