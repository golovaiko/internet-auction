import axios from "axios";
import authHeader from './auth-header';
import { handleError } from '../helpers/request.error.handler';
import configData from "../config.json";

const API_URL = `${configData.SERVER_URL}account/`;

class AuthService {

  login(username, password) {
    return axios
      .post(API_URL + "login", {
        email:username,
        password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(email, password , passwordConfirm, firstName, lastName) {
    return axios.post(API_URL + "register", {
      email,
      password,
      passwordConfirm,
      firstName,
      lastName
    });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }

  getAllUsers() {
    return axios.get(API_URL + "getAllUsers", { headers: authHeader() })
    .then(response => { if (response) return response; } )
    .catch(err => {return handleError(err)});
  }
}

export default new AuthService();