import axios from 'axios';
import authHeader from './auth-header';
import { handleError } from '../helpers/request.error.handler';
import configData from "../config.json";

const API_URL = `${configData.SERVER_URL}lot/`;

class LotsService {

  async getPublicContent() {
    return axios
      .get(API_URL + 'getSimpleLots')
      .then(response => { if (response) return response; } )
      .catch(err => { return handleError(err) });
  }

  async getArchivedLots() {
    return axios
      .get(API_URL + 'getArchivedSimpleLots', { headers : authHeader() })
      .then(response => { if (response) return response; } )
      .catch(err => { return handleError(err) });
  }

  async getSearchLots(searchWord){
    return axios.get(API_URL + `getSimpleLotsBySearchWord?searchWord=${searchWord}`)
    .then(response => { if (response) return response; } )
    .catch(err => { return handleError(err) });
  }

  async getLotDetailsById(id) {
    return axios
      .get(API_URL + `getLotById/${id}`, { headers: authHeader() })
      .then(response => { if (response) return response; } )
      .catch(err => { return handleError(err) });
  }

  async getLotHistoryById(id) {
    return axios
      .get(API_URL + `getHistoryForLot/${id}`, { headers: authHeader() })
      .then(response => { if (response) return response; } )
      .catch(err => { return handleError(err) });
  }

  async createLot(name, description , startPrice, buyoutPrice, minPriceStep, endDate, ownerEmail){
    return axios.post(API_URL + "createLot", {
      name,
      description,
      startPrice,
      buyoutPrice,
      minPriceStep,
      endDate,
      ownerEmail
    },{ headers: authHeader() })
    .catch(err => { return handleError(err) });
  }

  async makeAnOfferOnLot(id, offeredPrice, offererEmail){
    return axios.post(API_URL + "makeAnOfferOnLot", {
      id,
      offeredPrice,
      offererEmail
    },{ headers: authHeader() })
    .catch(err => { return handleError(err) });
  }

  async cancelLot(id , ownerEmail){
    return axios.post(API_URL + "cancelLot" , 
    {
      id,
      ownerEmail
    }, {headers: authHeader()})
    .catch(err => { return handleError(err) });
  }

  async buyoutLot(id , buyerEmail , price)
  {
    return axios.post(API_URL + "buyoutLot", 
    {
      id,
      buyerEmail,
      price
    }, {headers: authHeader()})
    .catch(err => { return handleError(err) });
  }

  async archiveLot(id)
  {
    return axios.post(API_URL + "archiveLot", 
    {
      id
    }, {headers: authHeader()})
    .catch(err => { return handleError(err) });
  }

  
}

export default new LotsService();