import React, { useState, useEffect } from "react";
import { Routes, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { useNavigate } from 'react-router-dom';

import AuthService from "./services/auth.service";

import Login from "./components/login.component";
import Register from "./components/register.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import LotDetails from "./components/lot.details.component";
import LotCreate from "./components/lot.create.component";
import LotsSearch from "./components/lot.search.component";
import Users from "./components/users.component";
import Error from "./components/error.component";
import LotsArchived from "./components/lots.archived.component";


export default function App() {
  let navigate = useNavigate();
  let [currentUser, setCurrentUser] = useState("");

  useEffect(() => {
    setCurrentUser(AuthService.getCurrentUser() )
  }, []);

  function logOut() {
    AuthService.logout();
    navigate("/");
    window.location.reload();
  }

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
        <Link to={"/"} className="navbar-brand">
          Auction
        </Link>
        {(currentUser && currentUser.roles[0] === "admin") ? 
            <Link to={"/listOfUsers"} className="navbar-brand">
              List of users
            </Link> 
            
          : null
        }
        {(currentUser && currentUser.roles[0] === "admin") ? 
            <Link to={"/lot/archived"} className="navbar-brand">
              Archived lots
            </Link> 
          : null
        }

        {currentUser ? (
          <div className="navbar-nav ms-auto">
            <ul className="nav navbar-nav flex-row">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  My Profile
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/"} className="nav-link" onClick={logOut}>
                  Log out
                </Link>
              </li>
            </ul>
          </div>
        ) : (
          <div className="navbar-nav ml-auto">
            <ul className="nav navbar-nav flex-row">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Register
                </Link>
              </li>
            </ul>
          </div>
        )}
        </div>
      </nav>

      <div className="container mt-3">
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/lot">
            <Route path=":id" element={<LotDetails/>}/>
            <Route path="createLot" element={<LotCreate/>}/>
            <Route path="search" element={<LotsSearch/>} />
            <Route path="archived" element={<LotsArchived/>}/>              
          </Route>
          <Route path="/login" element={<Login/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/profile" element={<Profile/>} />
          <Route path="/listOfusers" element={<Users/>}/>
          <Route path="/error" element={<Error/>}/>
        </Routes>
      </div>
    </div>
  );
}