export function handleError(error) {
    return { errorMessage: error.message }
}

export function validateResponse (error , navigate){
    if (!error)
    {
        return;
    }
    navigate(`/error?error=${error}`);
}