﻿namespace AuctionBLL.Account
{
    /// <summary>
    /// Class that represents a model in which data is received from user.
    /// </summary>
    /// <remarks>
    /// This model is used to assign user to an array of roles.
    /// </remarks>
    public class AssignUserToRoles
    {
        public string Email { get; set; }
        public string[] Roles { get; set; }
    }
}