﻿namespace AuctionBLL.Account
{
    /// <summary>
    /// Class that represents a model in which data is received from user.
    /// </summary>
    /// <remarks>
    /// This model is used to register a new user into the system.
    /// </remarks>
    public class Register
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}