﻿using AuctionBLL.DTO;
using AuctionDAL.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionBLL.Account
{
    /// <inheritdoc cref="IUserService" >
    public sealed class UserService : IUserService
    {
        private readonly UserManager<AuctionUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public readonly IMapper _mapper;

        /// <summary>
        /// Constructor for identifying User Manager , Role Manager and Mapper using dependency injections.
        /// </summary>
        /// <param name="userManager">User Manager from AspNetCore.Identity</param>
        /// <param name="roleManager">Role Manager from AspNetCore.Identity</param>
        /// <param name="mapper">Automapper for mapping entities into DTOs and back.</param>
        public UserService(UserManager<AuctionUser> userManager,
            RoleManager<IdentityRole> roleManager, IMapper mapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _mapper = mapper;
        }

        public async Task<bool> Register(Register user)
        {
            var newUser = new AuctionUser
            {
                Email = user.Email,
                UserName = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            var result = await _userManager.CreateAsync(newUser, user.Password);

            await _userManager.AddToRoleAsync(newUser, "user");

            if (!result.Succeeded)
            {
                return false;
            }
            return true;
        }

        public async Task<AuctionUser> Login(Login login)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == login.Email);
            if (user is null) 
                return null;

            return await _userManager.CheckPasswordAsync(user, login.Password) ? user : null;
        }

        public async Task<AuctionUser> GetUserByEmailAsync(string email)
        {
            return await _userManager.Users.SingleOrDefaultAsync(u => u.Email == email);
        }

        public async Task AssignUserToRoles(AssignUserToRoles assignUserToRoles)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == assignUserToRoles.Email);
            var roles = _roleManager.Roles.ToList().Where(r => assignUserToRoles.Roles.Contains(r.Name, StringComparer.OrdinalIgnoreCase))
                .Select(r => r.NormalizedName).ToList();

            var result = await _userManager.AddToRolesAsync(user, roles);

            if (!result.Succeeded)
            {
                throw new Exception(string.Join(';', result.Errors.Select(x => x.Description)));
            }
        }

        public async Task CreateRole(string roleName)
        {
            var result = await _roleManager.CreateAsync(new IdentityRole(roleName));

            if (!result.Succeeded)
            {
                throw new Exception($"Role could not be created: {roleName}.");
            }
        }

        public async Task<IEnumerable<IdentityRole>> GetRoles()
        {
            return await _roleManager.Roles.ToListAsync();
        }

        public async Task<IEnumerable<string>> GetRoles(AuctionUser user)
        {
            return (await _userManager.GetRolesAsync(user)).ToList();
        }

        public async Task<IEnumerable<AuctionUserDTO>> GetAllUsers()
        {
            return _mapper.Map<IList<AuctionUserDTO>>(await _userManager.Users.ToListAsync());
        }
    }
}
