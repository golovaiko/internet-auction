﻿using AuctionBLL.DTO;
using AuctionDAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionBLL.Account
{
    /// <summary>
    /// This interface describes management of users.
    /// </summary>
    /// <remarks>
    /// Interface describes all actions than can be done with users in this application and their roles.
    /// </remarks>
    public interface IUserService
    {
        /// <summary>
        /// Method that manages registration.
        /// </summary>
        /// <param name="user">Model , that comes from front-end, with data to register new user.</param>
        /// <returns>
        /// Returns a boolean value.
        /// </returns>
        Task<bool> Register(Register user);
        /// <summary>
        /// Method that manages login process.
        /// </summary>
        /// <param name="login">Model , that comes from front-end , with data to log in. </param>
        /// <returns>
        /// User who is trying to log in.
        /// </returns>
        Task<AuctionUser> Login(Login login);
        /// <summary>
        /// Method that allows to get user by email.
        /// </summary>
        /// <param name="email">Users email address.</param>
        /// <returns>
        /// User by email address.
        /// </returns>
        Task<AuctionUser> GetUserByEmailAsync(string email);
        /// <summary>
        /// Method that returns list of all existing users.
        /// </summary>
        /// <returns>
        /// List of all existing users.
        /// </returns>
        Task<IEnumerable<AuctionUserDTO>> GetAllUsers();
        /// <summary>
        /// Method that allows to assign user to an array of roles.
        /// </summary>
        /// <param name="assignUserToRoles"></param>
        /// <returns></returns>
        Task AssignUserToRoles(AssignUserToRoles assignUserToRoles);
        /// <summary>
        /// Method that allows to create a new user role.
        /// </summary>
        /// <param name="roleName">Name of new user role.</param>
        /// <returns></returns>
        Task CreateRole(string roleName);
        /// <summary>
        /// Method that returst list of roles for user.
        /// </summary>
        /// <param name="user">User that we want to know roles of.</param>
        /// <returns>
        /// List of chosen user roles.
        /// </returns>
        Task<IEnumerable<string>> GetRoles(AuctionUser user);
        /// <summary>
        /// Method returns list of all existing roles.
        /// </summary>
        /// <returns>
        /// List of all existing roles.
        /// </returns>
        Task<IEnumerable<IdentityRole>> GetRoles();
    }
}
