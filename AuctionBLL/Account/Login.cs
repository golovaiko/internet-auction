﻿namespace AuctionBLL.Account
{
    /// <summary>
    /// Class that represents a model in which data is received from user.
    /// </summary>
    /// <remarks>
    /// This model is used to login user into the system.
    /// </remarks>
    public class Login
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}