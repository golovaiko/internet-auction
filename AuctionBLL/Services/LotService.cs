﻿using AuctionBLL.Interfaces;
using AuctionBLL.DTO;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuctionDAL.Enums;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AuctionBLL.Services
{
    /// <inheritdoc cref="ILotService">
    public class LotService : ILotService
    {
        private readonly ILotRepository _lotRepository;
        private readonly ILotHistoryRepository _lotHistoryRepository;
        private readonly ILogger<LotService> _logger;

        private readonly IMapper _mapper;

        /// <summary>
        ///  Constructor for defining LotHistory and Lot repositories also Mapper and logger using dependency injections.
        /// </summary>
        /// <param name="lotRepository">Repository of Lot entity through which we are getting access to database.</param>
        /// <param name="lotHistoryRepository">Repository of LotHistory entity through which we are getting access to database.</param>
        /// <param name="mapper">Automapper for mapping entities into DTOs and back.</param>
        /// <param name="logger">Logger to log info about actions done on lots.</param>
        public LotService(ILotRepository lotRepository, IMapper mapper , ILotHistoryRepository lotHistoryRepository, ILogger<LotService> logger)
        {
            _lotHistoryRepository = lotHistoryRepository;
            _lotRepository = lotRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<int> AddAsync(LotDTO model, AuctionUser auctionUser)
        {
            var lotToAdd = _mapper.Map<Lot>(model);
            lotToAdd.Owner = auctionUser;

            await _lotRepository.AddAsync(lotToAdd);
            _logger.LogInformation($"{lotToAdd.Name} was created.");
            return lotToAdd.Id;
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _lotRepository.DeleteByIdAsync(id);
            _logger.LogInformation($"Lot with id={id} was archived.");
        }

        public IQueryable<LotSimpleDTO> FindAllArchivedSimple()
        {
            _logger.LogInformation("Simple archived lots was requested");
            return _mapper.Map<IList<LotSimpleDTO>>(_lotRepository.FindAllArchived()).AsQueryable();
        }

        public IQueryable<LotSimpleDTO> FindAllActiveSimple()
        {
            _logger.LogInformation("Simple active lots was requested.");
            return _mapper.Map<IList<LotSimpleDTO>>(_lotRepository.FindAllActive().AsNoTracking()).AsQueryable();
        }

        public IQueryable<LotSimpleDTO> FindAllActiveBySearchWord(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return new List<LotSimpleDTO>().AsQueryable();
            }

            _logger.LogInformation("Someone searched in lots.");
            return _mapper.Map<IList<LotSimpleDTO>>(_lotRepository.FindAllActiveBySearchWord(searchText)).AsQueryable();
        }

        public async Task<LotDTO> GetByIdAsync(int id)
        {
            var lot = await _lotRepository.GetByIdAsync(id);

            if (lot == null)
            {
                return null;
            }

            var lotDTO = _mapper.Map<LotDTO>(lot);
            var lastLotHistory = _lotHistoryRepository.GetHistoryByLotId(id).OrderByDescending(x => x.TransactionDate).FirstOrDefault();
            lotDTO.LastTransaction = lastLotHistory?.TransactionType ?? LotTransactionType.Opened;
            _logger.LogInformation($"Lot with id={id} got requested.");
            return lotDTO;
        }

        public async Task UpdateForOffer(LotDTO model, AuctionUser user)
        {
            var lotToUpdate = _mapper.Map<Lot>(model);

            lotToUpdate.CurrentBuyer = user;

            await _lotRepository.Update(lotToUpdate);
            _logger.LogInformation($"Lot with id={lotToUpdate.Id} got an offer.");
        }

        public async Task UpdateForBuyout(LotDTO model, AuctionUser user)
        {
            var lotToUpdate = _mapper.Map<Lot>(model);

            lotToUpdate.FinalPrice = lotToUpdate.BuyoutPrice;
            lotToUpdate.CurrentPrice = lotToUpdate.BuyoutPrice;
            lotToUpdate.CurrentBuyer = user;
            lotToUpdate.FinalBuyer = user;

            await _lotRepository.Update(lotToUpdate);
            _logger.LogInformation($"Lot with id={lotToUpdate.Id} was bought.");
        }

        public int CloseLots()
        {
            int lotsClosed = 0;
            var todaysDate = DateTime.UtcNow;

            var lotsToClose = _lotRepository.FindAllActive().Where(x => x.Status != LotStatus.ReadyForArchive && x.EndDate <= todaysDate);
            var lotHistory = new List<LotHistory>();
            foreach (var lot in lotsToClose)
            {
                lot.FinalBuyer = lot.CurrentBuyer;
                lot.FinalPrice = lot.CurrentPrice;
                lot.Status = LotStatus.ReadyForArchive;

                lotHistory.Add(new LotHistory()
                {
                    LotId = lot.Id,
                    TransactionDate = todaysDate,
                    TransactionType = LotTransactionType.Closed
                });

                lotsClosed++;
            }

            _lotHistoryRepository.AddRange(lotHistory);

            return lotsClosed;
        }
    }
}
