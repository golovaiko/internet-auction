﻿using AuctionBLL.Interfaces;
using AuctionBLL.DTO;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace AuctionBLL.Services
{
    /// <inheritdoc cref="ILotHistoryService">
    public class LotHistoryService : ILotHistoryService
    {
        private readonly ILotHistoryRepository _lotHistoryRepository;
        private readonly IMapper _mapper;

        /// <summary>
        ///  Constructor for defining LotHistory repository and Mapper using dependency injections.
        /// </summary>
        /// <param name="lotHistoryRepository">Repository of LotHistory entity through which we are getting access to database.</param>
        /// <param name="mapper">Automapper for mapping entities into DTOs and back.</param>
        public LotHistoryService(ILotHistoryRepository lotHistoryRepository, IMapper mapper)
        {
            _lotHistoryRepository = lotHistoryRepository;
            _mapper = mapper;
        }

        public async Task AddAsync(LotHistoryDTO model, AuctionUser user)
        {
            var lotHistory = _mapper.Map<LotHistory>(model);

            lotHistory.User = user;

            await _lotHistoryRepository.AddAsync(lotHistory);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _lotHistoryRepository.DeleteByIdAsync(id);
        }

        public IQueryable<LotHistoryDTO> FindAll()
        {
            return _mapper.Map<IList<LotHistoryDTO>>(_lotHistoryRepository.FindAll()).AsQueryable();
        }

        public IQueryable<LotHistoryDTO> GetHistoryByLotId(int lotId)
        {
            return _mapper.Map<IList<LotHistoryDTO>>(_lotHistoryRepository.GetHistoryByLotId(lotId)).AsQueryable();
        }
    }
}
