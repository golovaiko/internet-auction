﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionBLL.DTO
{
    public class AuctionUserDTO
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
