﻿using AuctionDAL.Entities;
using AuctionDAL.Enums;
using System;

namespace AuctionBLL.DTO
{
    public class LotHistoryDTO
    {
        public int LotId { get; set; }
        public string UserEmail { get; set; }
        public DateTime TransactionDate { get; set; }
        public LotTransactionType TransactionType { get; set; }
    }
}
