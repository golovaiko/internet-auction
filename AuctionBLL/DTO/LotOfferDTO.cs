﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AuctionBLL.DTO
{
    public class LotOfferDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string OffererEmail { get; set; }
        [Required]
        public double OfferedPrice { get; set; }
    }
}
