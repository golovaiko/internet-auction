﻿using AuctionDAL.Entities;
using AuctionDAL.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuctionBLL.DTO
{
    public class LotDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public LotStatus Status { get; set; }
        public LotTransactionType LastTransaction { get; set; }

        public double StartPrice { get; set; }
        public double? BuyoutPrice { get; set; }
        public double? CurrentPrice { get; set; }
        public double MinPriceStep { get; set; }
        public double? FinalPrice { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime EndDate { get; set; }
        public string OwnerEmail { get; set; }
        public string FinalBuyerEmail { get; set; }
        public string CurrentBuyerEmail { get; set; }
    }
}