﻿namespace AuctionBLL.DTO
{
    public class LotSimpleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
