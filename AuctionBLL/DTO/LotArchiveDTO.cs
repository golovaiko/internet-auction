﻿using System.ComponentModel.DataAnnotations;

namespace AuctionBLL.DTO
{
    public class LotArchiveDTO
    {
        [Required]
        public int Id { get; set; } 
    }
}
