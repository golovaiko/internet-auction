﻿using AuctionBLL.DTO;
using AuctionDAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    /// <summary>
    /// This interface describes actions that can be done with LotHistory entity.
    /// </summary>
    public interface ILotHistoryService
    {
        /// <summary>
        /// Method that returns a list of all existing LotHistories.
        /// </summary>
        /// <returns>
        /// List of all existing LotHistories.
        /// </returns>
        IQueryable<LotHistoryDTO> FindAll();
        /// <summary>
        /// Method returns all existing LotHistores for chosen lot.
        /// </summary>
        /// <param name="lotId">Id of chosen lot.</param>
        /// <returns>
        /// List of all existing LotHistories for chosen lot.
        /// </returns>
        IQueryable<LotHistoryDTO> GetHistoryByLotId(int lotId);
        /// <summary>
        /// Method adds a new LotHistory.
        /// </summary>
        /// <param name="model">Model of new LotHistory.</param>
        /// <param name="user">User that is trying to do an action on a lot.</param>
        Task AddAsync(LotHistoryDTO model, AuctionUser user);
        /// <summary>
        /// Method deletes a LotHistory record from database by id.
        /// </summary>
        /// <param name="id">Id of LotHistory record to delete.</param>
        /// <returns></returns>
        Task DeleteByIdAsync(int id);
    }
}
