﻿using AuctionBLL.DTO;
using AuctionDAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    /// <summary>
    /// This interface describes actions that can be done with Lot entity.
    /// </summary>
    public interface ILotService
    {
        /// <summary>
        /// Method returns list of all archived lots in simple form: title and description.
        /// </summary>
        /// <returns>
        /// List of all archived lots in simple form.
        /// </returns>
        IQueryable<LotSimpleDTO> FindAllArchivedSimple();
        /// <summary>
        /// Method returns list of all active lots in simple form: title and description.
        /// </summary>
        /// <returns>
        /// List of all active lots in simple form.
        /// </returns>
        IQueryable<LotSimpleDTO> FindAllActiveSimple();
        /// <summary>
        /// Method returns list of all active lots in simple form: title and description.
        /// </summary>
        /// <returns>
        /// List of all active lots in simple form, that contain search string in title or description.
        /// </returns>
        IQueryable<LotSimpleDTO> FindAllActiveBySearchWord(string searchText);
        /// <summary>
        /// Method gets a lot.
        /// </summary>
        /// <param name="id">Id of chosen lot</param>
        /// <returns>
        /// Chosen lot by it's id.
        /// </returns>
        Task<LotDTO> GetByIdAsync(int id);
        /// <summary>
        /// Method adds a new lot to database.
        /// </summary>
        /// <param name="model">Model of new lot to create.</param>
        /// <param name="auctionUser">User that is trying to create lot.</param>
        /// <returns>
        /// New lot id.
        /// </returns>
        Task<int> AddAsync(LotDTO model, AuctionUser auctionUser);
        /// <summary>
        /// Method for updating lot when someone making a bet on a lot.
        /// </summary>
        /// <param name="model">Model of a lot.</param>
        /// <param name="user">User that makes a bet.</param>
        Task UpdateForOffer(LotDTO model, AuctionUser user);
        /// <summary>
        /// Method for updating lot when someone buying a lot.
        /// </summary>
        /// <param name="model">Model of a lot.</param>
        /// <param name="user">User that buies.</param>
        Task UpdateForBuyout(LotDTO model, AuctionUser user);
        /// <summary>
        /// Method for updating lot when admin is archiving it.
        /// </summary>
        /// <param name="id">Id of the lot</param>
        Task DeleteByIdAsync(int id);
        /// <summary>
        /// Method that closes lots when date of the auction end comes.
        /// </summary>
        /// <returns>
        /// Number of lots closed.
        /// </returns>
        int CloseLots();

    }
}
